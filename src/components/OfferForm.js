import React from 'react'
import InputMask from "react-input-mask";
import icoFullName from '../img/ico-fullname.svg'
import icoPhone from '../img/ico-phone.svg'
import icoEmail from '../img/ico-email.svg'
import icoAddress from '../img/ico-address.svg'
import sent from '../img/img-sent.svg'

const encode = (data) => {
  return Object.keys(data).map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key])).join("&");
}

const initialState = {
  form: {
    name: "",
    phone: "",
    email: "",
    address: ""
  },
  messageSent: false
};

class OfferFormClass extends React.Component {

  constructor(props) {
    super();
    this.state = initialState;
  }

  resetState() {
    this.setState(initialState);
  }

  handleSubmit = e => {
    e.preventDefault();

    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({ "form-name": "cash-offer", ...this.state.form })
    })
      .then(() => this.setState({ messageSent: true }))
      .catch(error => alert(error));
  }

  handleChange = e => {
    let form = { ...this.state.form }
    if (e.target){
      form[e.target.name] = e.target.value;
    }
    this.setState({ form })
  };

  render() {

    if (!this.state.messageSent){

      const { name, phone, email, address } = this.state.form;
      return (
        <div className="form-holder text-center">

          <h2 className="text-36 font-extrabold text-dark-gray mb-4">Interested in a cash offer?</h2>
          <p className="max-w-xl leading-relaxed mx-auto mb-8">Simply fill out this form and one of our home buying experts will valuate your home. We’ll be in touch to discuss our offer within 48 hours.</p>

          <div className="cashoffer-form max-w-xl mx-auto">
            <form
              onSubmit={this.handleSubmit}
              name="cash-offer"
              data-netlify="true"
              data-netlify-honeypot="bot-field"
              className="cash-offer"
            >
              <input aria-label="Form name static field" type="hidden" name="form-name" value="cash-offer" />
              <p className="hidden">
                <label>Don’t fill this out if you're human: <input aria-label="Antibot static field" name="bot-field" /></label>
              </p>

              <div className="form-group">
                <div className="iconbox">
                  <img src={icoFullName} alt="Full Name" />
                </div>
                <input aria-label="Name" type="text" value={name} onChange={this.handleChange} id="name" name="name" className="form-control" required />
                <label htmlFor="name" className="floating-label">Full Name</label>
              </div>

              <div className="form-group">
                <div className="iconbox">
                  <img src={icoPhone} alt="Phone" />
                </div>
                <InputMask aria-label="Phone Number" className="form-control" value={phone} onChange={this.handleChange} mask="(999) 999-9999" name="phone" id="phone" required />
                <label htmlFor="phone" className="floating-label">Phone Number</label>
              </div>

              <div className="form-group">
                <div className="iconbox">
                  <img src={icoEmail} alt="Email" />
                </div>
                <input aria-label="Email address" type="email" value={email} onChange={this.handleChange} id="email" name="email" className="form-control" required />
                <label htmlFor="email" className="floating-label">Email Address</label>
              </div>

              <div className="form-group pb-3">
                <div className="iconbox">
                  <img src={icoAddress} alt="Full property address" />
                </div>
                <input aria-label="Name" type="text" value={address} onChange={this.handleChange} id="address" name="address" className="form-control" required />
                <label htmlFor="address" className="floating-label">Full property address</label>
              </div>

              <button className="py-8 leading-none font-bold rounded-50 text-white bg-blue px-20 text-18 w-full mb-8" type="submit">Get a Cash Offer</button>
            </form>
            <span className="text-center uppercase text-14 block mb-16">Your information is kept strictly private</span>
          </div>
        </div>
      )
    }
    else{
      return (
        <div className="memberbox request-sent text-center py-36">
          <img className="inline-block mb-10" src={sent} alt=""/>
          <h2 className="text-36 font-extrabold text-dark-gray mb-4">Request Sent!</h2>
          <p className="max-w-3xl leading-relaxed mx-auto mb-8">Thank you for submitting your home buying request! One of our market specialists will review your property and contact you as soon as possible to discuss your options</p>
        </div>
      )
    }
  }
}
export default function OfferFrom(){
  return (
    <OfferFormClass />
  )
}
