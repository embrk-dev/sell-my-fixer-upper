import React from 'react'
import { Link } from "gatsby"
import logo from '../img/logo.png'
import facebook from '../img/facebook.svg'
import instagram from '../img/instagram.svg'
import "../styles/mobilenav.scss"

function MyVerticallyCenteredModal(props) {
  if (props.show){
    return (
      <div id="mobileNav">
        <Link to="/" className="text-20 text-dark-gray flex items-center logo" title="Sell My Fixer Upper">
          <img className="w-44" src={logo} alt="Sell My Fixer Upper" />
          <strong className="ml-3">Sell My <span className="text-blue">Fixer Upper</span></strong>
        </Link>
        <div className="mobileNav-inner px-4">
          <ul className="nav">
            <li><Link to="/">Home</Link></li>
            <li><Link to="/howitworks/">How it Works</Link></li>
            <li><Link to="/ourprojects/">Our Projects</Link></li>
            <li><Link to="/aboutus/">About</Link></li>
          </ul>
          <div className="flex justify-center">
            <a className="mx-6" title="facebook" href={props.contacts.facebook} target="_blank" rel="noreferrer">
              <img
                src={facebook}
                alt="Facebook"
                style={{ width: '2.8rem', height: '2.8rem' }}
              />
            </a>
            <a className="mx-6" title="instagram" href={props.contacts.instagram} target="_blank" rel="noreferrer">
              <img
                src={instagram}
                alt="Instagram"
                style={{ width: '2.8rem', height: '2.8rem' }}
              />
            </a>
          </div>
        </div>
          <p className="phone absolute bottom-0 left-0 right-0 text-center font-bold text-22 text-dark-gray mb-20"><span className="block">Call or Text</span><a href={'tel:' + props.contacts.phone}>{props.contacts.phone}</a><span className="block">&nbsp;</span> <span className="block">Call Toll-Free</span><span className="block"><a href="tel:+18447355693">(844) 735-5693</a></span> </p>
        <button
            aria-label="Close Modal"
            onClick={props.onHide}
            onKeyDown={props.onHide}
            type="button"
            className="close"
            data-dismiss="modal"
          ><svg width="20" height="19" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill="#7B8794" d="M2.31 0l16.918 16.918-1.538 1.538L.773 1.538z"/><path fill="#7B8794" d="M19.228 1.54L2.311 18.456.773 16.919 17.69.001z"/></svg></button>
      </div>
    );
  }
  else{
    return null
  }
}


const MobileNav = ({data}) => {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <button
        className="openNav"
        onClick={() => setModalShow(true)}
        onKeyDown={() => setModalShow(true)}
        >
          <span></span>
      </button>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        contacts={data}
      />
    </>
  );
}

export default MobileNav
