import React from "react"
import ReactMarkdown from 'react-markdown'
import { useStaticQuery, graphql } from 'gatsby'
import PropTypes from 'prop-types'
import "../styles/testimonials.scss"
import Img from 'gatsby-image'
import stars from '../img/stars.svg'

function Testimonials({ showHeader }) {

  const pagedata = useStaticQuery(graphql`
    query {
      markdownRemark(frontmatter: { templateKey: { eq: "testimonials-page" } }){
        frontmatter{
          testimonialsList{
            name
            text
            whenPosted
            image{
              childImageSharp {
                fixed(width:58) {
                  ...GatsbyImageSharpFixed_noBase64
                }
              }
              publicURL
            }
          }
        }
      }
    }
  `)

  const testimonials = pagedata.markdownRemark.frontmatter.testimonialsList;

  return (
    <React.Fragment>
      {!showHeader &&
        <div className="md:text-center md:pb-28">
          <strong className="uppercase font-semibold md:text-18 text-14 text-blue block mb-4">Testimonials</strong>
          <h3 className="md:text-38 text-24 font-extrabold mb-6 text-dark-gray">You’re not alone! In fact, we’ve helped thousands of sellers.</h3>
        </div>
      }
      <div className="flex justify-between flex-wrap">
        {testimonials.map((node, index) => (
        <div key={index} className="md:w-flex1/2 bg-white rounded-20 box-shadow p-14 border-box mb-12">
          <img className="mb-14 block" src={stars} alt="" />
          <div className="testimonial-text">
              <ReactMarkdown source={node.text}/>
          </div>
          <div className="flex items-center">
            {node.image.childImageSharp
              ? <Img className="mr-6 rounded-full" fixed={node.image.childImageSharp.fixed} />
              : <img className="mr-6 rounded-full" src={node.image.publicURL} alt={node.name} style={{ width: '60px', height: '60px' }}/>
            }
            <div className="text-18">
              <strong className="font-bold block text-dark-gray">{node.name}</strong>
              <span className="opacity-50 ">{node.whenPosted}</span>
            </div>
          </div>
        </div>
        ))}
      </div>
    </React.Fragment>
  )
}

Testimonials.propTypes = {
  showHeader: PropTypes.bool,
}

export default Testimonials
