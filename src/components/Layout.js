import React from 'react'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import { useStaticQuery, graphql } from 'gatsby'

const TemplateWrapper = ({ children }) => {

  const pagedata = useStaticQuery(graphql`
    query {
      settingsData: markdownRemark(frontmatter: { templateKey: { eq: "settings-page" } }){
        frontmatter{
          contacts{
            phone
            address
            email
            facebook
            instagram
          }
        }
      }
      cities: allMarkdownRemark(filter: {frontmatter: {templateKey: {eq: "city-page"}}}) {
        edges {
          node {
            frontmatter {
              title
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `)

  const cities = pagedata.cities.edges.sort(function (a, b) {
    let nameA = a.node.frontmatter.title.toUpperCase();
    let nameB = b.node.frontmatter.title.toUpperCase();
    if (nameA < nameB) { return -1; }
    if (nameA > nameB) { return 1; }
    return 0;
  });

  return (
    <React.Fragment>
      <Navbar data={pagedata.settingsData} cities={cities} />
      <div>{children}</div>
      <Footer page={children.type ? children.type.name : null} data={pagedata.settingsData}  cities={cities} />
    </React.Fragment>
  )
}

export default TemplateWrapper
