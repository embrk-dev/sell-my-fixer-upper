import React from 'react'
import { Link } from 'gatsby'
import MobileNav from '../components/MobileNav'

import logo from '../img/logo.png'
import facebook from '../img/facebook.svg'
import { FaChevronDown } from 'react-icons/fa';
import instagram from '../img/instagram.svg'

function Navbar({ data, cities }) {
  const contacts = data.frontmatter.contacts;

  return (
    <nav
      className="sm:px-12 px-4 py-5 flex justify-between items-center relative z-100"
      role="navigation"
      aria-label="main-navigation"
    >
      <div className="flex items-center">
        <h1>
          <Link to="/" className="sm:text-26 text-20 text-dark-gray flex items-center" title="Sell My Fixer Upper">
            <img className="w-44 sm:w-60" src={logo} alt="Sell My Fixer Upper" />
            <strong className="ml-3">Sell My <span className="text-blue">Fixer Upper</span></strong>
          </Link>
        </h1>
        <div className="sm:flex hidden items-center pl-10 t-2">
          <a className="ml-3" title="facebook" href={contacts.facebook} target="_blank" rel="noreferrer">
            <img
              src={facebook}
              alt="Facebook"
              style={{ width: '1.7rem', height: '1.7rem' }}
            />
          </a>
          <a className="ml-3" title="instagram" href={contacts.instagram} target="_blank" rel="noreferrer">
            <img
              src={instagram}
              alt="Instagram"
              style={{ width: '1.7rem', height: '1.7rem' }}
            />
          </a>
        </div>
      </div>
      <div
        id="navMenu"
        className="text-18 text-dark-gray flex flex-row items-center"
      >
        <div className="text-gray mr-16 drop-holder flex">
          <span className="flex items-center">Locations <FaChevronDown className="text-11 ml-3" /></span>
          <div>
            <ul>
              {cities.map((item, i) => (
              <li className="mb-2" key={i}><Link  to={ `/${item.node.fields.slug.split('/')[2]}` }>{ item.node.frontmatter.title }</Link></li>
              ))}
            </ul>
          </div>
        </div>
        <Link className="text-gray mr-16 hover:text-blue" to="/scenarios/foreclosure">Scenario</Link>
        <Link className="text-gray mr-16 hover:text-blue" to="/howitworks">How it Works</Link>
        <Link className="text-gray mr-16 hover:text-blue" to="/ourprojects">Our Projects</Link>
        <Link className="mr-16 text-gray hover:text-blue" to="/aboutus">About</Link>
          <div className="flex flex-col text-center">
            <strong>Call or Text {contacts.phone}</strong>
            <strong>Toll Free 1-844-735-5693</strong>
          </div>
      </div>
      <MobileNav data={contacts} />
    </nav>
  )
}

export default Navbar
