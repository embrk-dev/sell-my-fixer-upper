import React from "react"

import { useStaticQuery, graphql } from 'gatsby';
import "../styles/faq.scss"
import {Disclosure} from "@headlessui/react";
import {ChevronDownIcon} from "@heroicons/react/outline";


export default function Faq() {
    const pagedata = useStaticQuery(graphql`
    query {
      markdownRemark(frontmatter: { templateKey: { eq: "faqs-page" } }){
        frontmatter{
          faqs{
            title
            text
          }
        }
      }
    }
  `)

    const faqs = pagedata.markdownRemark.frontmatter.faqs;


    function classNames(...classes) {
        return classes.filter(Boolean).join(' ')
    }


    return (
        <React.Fragment>
            <h3 className="md:text-36 text-24 font-extrabold mb-16 text-dark-gray">Common Questions About Selling Your Fixer Upper</h3>
            <dl className="mt-6 space-y-6 divide-y divide-gray-200">

                {faqs.map((faq, index) => (
                    <Disclosure as="div" key={index} className="pt-6">
                        {({ open }) => (
                            <>
                                <dt className="text-lg">
                                    <Disclosure.Button className="text-left w-full flex justify-between items-start text-gray-400 border-b border-medium-gray" style={{ outline: "none", "border-bottom": "1px solid #e4e7eb"}}>
                                        <span className="font-semibold text-dark-gray text-19 pb-5 ">{ faq.title }</span>
                                        <span className="ml-6 h-7 flex items-center">
                          <ChevronDownIcon
                              className={classNames(open ? '-rotate-180' : 'rotate-0', 'h-12 w-12 transform')}
                              aria-hidden="true"
                          />
                        </span>
                                    </Disclosure.Button>
                                </dt>
                                <Disclosure.Panel as="dd" className="mt-2 pr-12 max-w-6xl">
                                    <p className="text-base text-gray text-18">{ faq.text }</p>
                                </Disclosure.Panel>
                            </>
                        )}
                    </Disclosure>
                ))}
            </dl>
        </React.Fragment>
    )
}
