import React from "react"
import ReactTooltip from 'react-tooltip';
import { useStaticQuery, graphql } from 'gatsby'
import "../styles/sellwithconfidence.scss"

import logo from '../img/logo.png'
import checkGreen from '../img/check-green.svg'
import checkGray from '../img/check-gray.svg'
import question from '../img/question.svg'

function Sellwithconfidence() {

  const pagedata = useStaticQuery(graphql`
    query {
      markdownRemark(frontmatter: { templateKey: { eq: "sellwithconfidence-page" } }){
        frontmatter{
          sellwithconfidence {
            title
            heading
            table{
              rowTitle
              smfuText
              smfuTooltipText
              taText
            }
          }
        }
      }
    }
  `)
  const sellwithconfidenceData = pagedata.markdownRemark.frontmatter.sellwithconfidence;

  return (
    <div className="md:pb-32 pb-8">
      <div className="md:text-center pb-20">
        <strong className="uppercase font-semibold md:text-18 text-14 text-blue block mb-4">{ sellwithconfidenceData.title}</strong>
        <h3 className="md:text-38 text-24 font-extrabold md:mb-6 text-dark-gray">{ sellwithconfidenceData.heading}</h3>
      </div>
      <table id="comparison-table" className="w-full font-medium">
        <thead>
          <tr>
            <th> </th>
            <th>
              <span className="text-20 text-black flex items-center justify-center">
                <img src={logo} alt="Sell My Fixer Upper" style={{ width: '100px' }} />
                <strong className="ml-3 md:block hidden">Sell My <span className="text-blue">Fixer Upper</span></strong>
              </span>
            </th>
            <th><h4 className="md:text-24 text-16 font-extrabold text-dark-gray">Traditional Agent</h4></th>
          </tr>
        </thead>
        <tbody>
          {sellwithconfidenceData.table.map((node, i) => (
          <tr key={i}>
            <td>{ node.rowTitle }</td>
            <td>
                <div className="mdl-holder">
                <img
                  className="mr-6"
                  src={checkGreen}
                  alt=""
                  style={{ width: '16px', height: '13px' }}
                />
                { node.smfuText }
                {node.smfuTooltipText
                  ? <React.Fragment><span className="ml-auto md:inline-block hidden" data-tip={node.smfuTooltipText}><img src={question} alt="" style={{ width: '13px', height: '13px' }} /></span><ReactTooltip place="right" type="dark" effect="solid" /></React.Fragment>
                  : ''
                }
              </div>
            </td>
            <td>
              <div className="mdl-holder">
                <img
                  className="mr-6"
                  src={checkGray}
                  alt=""
                  style={{ width: '16px', height: '13px' }}
                />
                { node.taText }
              </div>
            </td>
          </tr>
          ))}
        </tbody>
        <tfoot>
          <tr>
            <td colSpan="3" className="text-12 text-dark-gray md:text-right text-center">* Assuming the house sold for $500K</td>
          </tr>
        </tfoot>
      </table>
    </div>
  )
}
export default Sellwithconfidence
