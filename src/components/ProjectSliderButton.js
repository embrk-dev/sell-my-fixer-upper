import React from "react";
import { useLightbox } from "simple-react-lightbox";
import arrow from "../img/arrow.svg"

const ProjectSliderButton = (props) => {
  const { openLightbox } = useLightbox();

  return (
    <button
      className="cursor-pointer text-blue font-medium"
      onClick={() => openLightbox(props.imageToOpen)}
    >
      View Gallery <img className="inline-block ml-3" src={arrow} alt=""/>
    </button>
  );
};

export default ProjectSliderButton;
