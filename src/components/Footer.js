import React from 'react'
import { Link } from 'gatsby'
import Lightbox from "../components/Lightbox"
import facebook from '../img/facebook-footer.svg'
import instagram from '../img/instagram-footer.svg'
import OfferForm from '../components/OfferForm'

function Footer ({ page, data, cities }) {
  const contacts = data.frontmatter.contacts;
  const isHome = page ? false : true;
  const isCity = page !== 'CityPageTemplate' ? false : true;
  return (
    <footer id="footer" className={`${isHome ? 'homepage' : 'not-homepage'} ${isCity && 'citypage' } bg-blue text-white relative`}>
      <div className="max-w-page mx-auto pb-16 px-6">
        {!isCity &&
          <div className="md:pb-56 pb-40 text-center max-w-5xl mx-auto">
            <h3 className="md:text-38 text-34 font-extrabold mb-6 sm:leading-normal leading-tight">Get a fast & fair cash offer quote!</h3>
            <p className="md:text-20 text-16 md:mb-14 mb-8">When you are ready, just fill out our request form or give us a call to begin. We will immediately return an offer.</p>
            <Lightbox />
          </div>
        }
        <ul className="flex flex-wrap justify-center items-center md:flex-row flex-col">
          <li className="px-6 pb-6"><Link className="text-white" to="/">Home</Link></li>
          <li className="px-6 pb-6"><Link className="text-white" to="/howitworks">How it Works</Link></li>
          <li className="px-6 pb-6"><Link className="text-white" to="/ourprojects">Our Projects</Link></li>
          <li className="px-6 pb-6"><Link className="text-white" to="/aboutus">About</Link></li>
        </ul>
        <ul className="text-center">
        {cities.map((item, index) => (
          <li key={index} className="px-6 pb-6 inline-block"><Link className="text-white" to={ `/${item.node.fields.slug.split('/')[2]}` }>{ item.node.frontmatter.title }</Link></li>
        ))}
        </ul>
        <div className="flex flex-wrap justify-center pb-6">
          <a title="facebook" href={contacts.facebook} target="_blank" rel="noreferrer">
            <img
              src={facebook}
              alt="Facebook"
              style={{ width: '1.7rem', height: '1.7rem' }}
            />
          </a>
          <a className="ml-3" title="instagram" href={contacts.instagram} target="_blank" rel="noreferrer">
            <img
              src={instagram}
              alt="Instagram"
              style={{ width: '1.7rem', height: '1.7rem' }}
            />
          </a>
        </div>
        <address className="not-italic text-center">{contacts.address}</address>
        <p className="text-center"><a href={'tel:' + contacts.phone}>{contacts.phone}</a><span className="inline-block mx-3">|</span> <a className="text-white" href={`mailto:${contacts.email}`}>{contacts.email}</a></p>
      </div>
      <div className="hidden">
        <OfferForm />
      </div>
    </footer>
  )
}

export default Footer
