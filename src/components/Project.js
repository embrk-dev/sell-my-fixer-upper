import React from "react"
import SimpleReactLightbox, { SRLWrapper } from "simple-react-lightbox"
import ProjectSliderButton from "../components/ProjectSliderButton"
import Img from 'gatsby-image'

function Project({ data }) {

  return (
    <React.Fragment>
      <div className="flex justify-between md:flex-row flex-col">
        <div className="md:w-flex1/2 w-full mb-8 photos-holder">
          <SimpleReactLightbox>
            <SRLWrapper>
            {data.imagesBefore.map((img, imgIndex) => {
              if (img.image)
                return (
                  <a
                    className={`${
                      imgIndex > 0 ? 'hidden' : 'block'
                    } rounded-10`}
                    key={imgIndex}
                    href={img.image.publicURL}
                  >
                    <img
                      className="block w-full h-full rounded-10"
                      src={img.image.publicURL}
                      alt=""
                    />
                  </a>
                );
            })}
            </SRLWrapper>
            {data.imagesBefore.length > 1 && <div className="text-center pt-8"> <ProjectSliderButton imageToOpen="0" /> </div>}
          </SimpleReactLightbox>
        </div>
        <div className="md:w-flex1/2 w-full mb-8 photos-holder">
          <SimpleReactLightbox>
            <SRLWrapper>
              {data.imagesAfter.map((img, imgIndex) => {
                if (img.image) return (<a className={`${imgIndex > 0 ? 'hidden' : 'block'} rounded-10`} key={imgIndex} href={img.image.publicURL}>
                  <img className="block w-full h-full rounded-10" src={img.image.publicURL} alt="" />
                </a>)
              })}
            </SRLWrapper>
            {data.imagesAfter.length > 1 && <div className="text-center pt-8"> <ProjectSliderButton imageToOpen="0" /> </div>}
          </SimpleReactLightbox>
        </div>
      </div>
      <div className="text-center">
        <h4 className="text-dark-gray text-20 font-medium">{data.title}</h4>
        <h5 className="uppercase font-medium text-16 py-4">{data.state}</h5>
        { data.text && <p className="sm:text-18 text-16 mb-8">{data.text}</p> }
        {/* { data.images.length > 2 && <ProjectSliderButton imageToOpen="0" /> } */}
      </div>
    </React.Fragment>
  )
}
export default Project
