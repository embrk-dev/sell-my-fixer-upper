import React from 'react'
import { Helmet } from 'react-helmet'
import { withPrefix } from 'gatsby'


const Seo = ({data}) => {
  return (
    <Helmet>
      <html lang="en" />
      <title>{data.browserTitle ? data.browserTitle : 'Sell My Fixer Upper'}</title>
      <meta name="title" content={data.metaTitle ? data.metaTitle : 'Sell My Fixer Upper'} />
      <meta name="description" content={data.descriptionTitle ? data.descriptionTitle : 'Sell My Fixer Upper'} />
      <link
          rel="apple-touch-icon"
          sizes="180x180"
          href={`${withPrefix('/')}img/apple-touch-icon.png`}
        />
        <link
          rel="icon"
          type="image/png"
          href={`${withPrefix('/')}img/favicon-32x32.png`}
          sizes="32x32"
        />
        <link
          rel="icon"
          type="image/png"
          href={`${withPrefix('/')}img/favicon-16x16.png`}
          sizes="16x16"
        />

        <link
          rel="mask-icon"
          href={`${withPrefix('/')}img/safari-pinned-tab.svg`}
          color="#ff4400"
        />

        <link
          rel="manifest"
          href={`${withPrefix('/')}img/site.webmanifest`}
          color="#ff4400"
        />
        <meta name="msapplication-TileColor" content="#da532c"/>
        <meta name="theme-color" content="#ffffff"/>

        <meta property="og:type" content="business.business" />
        <meta property="og:title" content={data.browserTitle ? data.browserTitle : 'Sell My Fixer Upper'} />
        <meta property="og:url" content="/" />
        <meta
          property="og:image"
          content={`${withPrefix('/')}img/sell-my-fixer-upper-logo.webp`}
        />
    </Helmet>
  )
}

export default Seo
