import React from 'react'
import OfferForm from '../components/OfferForm'
import { Modal } from 'react-bootstrap';
import PropTypes from 'prop-types'
import "../styles/lightbox.scss"

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      dialogClassName="custom-modal"
    >
      <Modal.Header>
        <button
          onClick={props.onHide}
          onKeyDown={props.onHide}
          type="button"
          className="close"
          data-dismiss="modal"
        >&times;</button>
      </Modal.Header>
      <Modal.Body>
        <OfferForm  />
      </Modal.Body>
    </Modal>
  );
}


const Lightbox = ({ btnText, btnClass }) => {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <button
        className={`${btnClass ? btnClass : 'py-8 leading-none font-bold rounded-50 text-blue bg-white px-20 text-18'}`}
        onClick={() => setModalShow(true)}
        onKeyDown={() => setModalShow(true)}
      >
        {btnText ? btnText : 'Get a Cash Offer'}
      </button>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
}

Lightbox.propTypes = {
  btnText: PropTypes.string,
  btnClass: PropTypes.string,
}

export default Lightbox
