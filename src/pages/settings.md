---
templateKey: settings-page
contacts:
  facebook: https://www.facebook.com/sellmyfixerupper
  instagram: https://www.instagram.com/sellmyfixerupper/
  address: PO Box 5676, Norman, OK 73070
  phone: (405) 792-0104
  email: sales@sellmyfixerupper
---
