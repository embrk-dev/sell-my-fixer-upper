---
path: /aboutus
templateKey: aboutus-page
seo:
  browserTitle: Sell My Fixer Upper
  metaTitle: Sell My Fixer Upper
  descriptionTitle: Sell My Fixer Upper
promo:
  title: About Us
  heading: Meet our
  headingMarked: team of experts
aboutus:
  heading: Sell My Fixer Upper is a real estate solutions company based out of Norman.
  text: >-
    Justin and his team have been buying homes and apartments in the OKC metro
    area since 2003. We are a direct buyer. We are a family-owned and operated
    company. We are not a franchise.


    Our passion is helping homeowners get out of sticky situations, whatever the cause may be, while simultaneously salvaging some of the beautiful homes in our city. We provide win-win solutions to help homeowners get out of their sticky situations… like fire damage, flood damage, condemnation, being stuck with terrible tenants, probate, or anything else.


    At Sell My Fixer Upper, we work to provide you with a STRESS-FREE solution to your situation so you can continue to do the things you love.
  image: /img/photo.jpg
displayTeam: false
ourTeam:
  heading: Our team
  text: Our team of experts have been helping homeowners since 2003. We want to
    help relieve you of any financial burden by purchasing your fixer upper.
  teamMembers:
    - name: Justin Gentry
      position: Owner
      photo: /img/justin-gentry.png
      text: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris a neque
        sed nulla interdum interdum. Cras fringilla tellus quam, sit amet
        gravida nisi mollis efficitur.
    - name: Zach Lorenzen
      position: Acquisitions
      text: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris a neque
        sed nulla interdum interdum. Cras fringilla tellus quam, sit amet
        gravida nisi mollis efficitur.
      photo: /img/zach-lorenzen.png
---
