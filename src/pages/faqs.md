---
templateKey: faqs-page
faqs:
  - title: Where do you extend your services?
    text: Our home base is in Norman, Oklahoma.  We buy in the entire Oklahoma City
      metro area from Shawnee to El Reno and from south of Noble to north
      Edmond.
  - title: What type of properties will you buy?
    text: We often buy the worst of the worst! We buy homes, apartment buildings,
      and land.
  - title: How will you view and appraise the home?
    text: After a quick phone call, we will set a time to come view the
      property.  If our sellers aren’t comfortable with someone coming in their
      home, we can do this virtually as well! We’ve bought many houses without
      having seen the property.
  - text: We have closed in as little as a couple of days. We have also set closings
      several months in the future, so the seller has time to make
      preparations.  This really depends on what the seller prefers.
    title: How long does the process take?
  - text: At closing!
    title: When can I expect payment?
  - text: We can pay all the closing costs if needed!
    title: What fees am I, the seller, responsible for?
  - title: What if title isn’t clear or probate is needed?
    text: We have attorneys to help with this, without paying a dime upfront.
  - title: What if the house is full of trash?
    text: We tell people to take what you want, and we’ll deal with the rest!
---
