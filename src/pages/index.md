---
templateKey: index-page
seo:
  browserTitle: Sell My Fixer Upper
  metaTitle: Sell My Fixer Upper
  descriptionTitle: Sell My Fixer Upper
promo:
  title: We Buy Homes In Any Condition
  heading: We make it easy to
  headingMarked: sell that fixer upper!
  text:
    Don’t waste money on repairs! No matter its current state, we’ll give you
    a fair cash offer for your home in and around Oklahoma City Metro.
  image: /img/img-video.png
  video: https://video.wixstatic.com/video/06d70a_707beb08b8994c1da97bb85e876a92ec/480p/mp4/file.mp4
howitworks:
  title: How It Works
  heading: It’s as easy as 1,2,3
  text: We make it easy to sell your home. In just 3 simple steps, we will
    appraise your home and close within days. Better yet, we buy homes in any
    condition all on your timeline. When we make an offer, you choose the
    closing date.
  howitworksList:
    - text: 1. Tell us about the property
      image: /img/property.svg
    - text: 2. Receive a fair, cash offer
      image: /img/cash-offer.svg
    - text: 3. Close when you’re ready
      image: /img/calendar.svg
  image: /img/img1.png
ourpromise:
  title: Our Promise
  heading: We buy properties in any condition
  text: Whatever your reason for selling, we want to help. We aim to make the
    process simple, so you can move on quickly.
  ourpromiseList:
    - name: Expensive repairs
      text:
        No need to spend additional money making the home presentable. We’ll buy
        as-is and take on the repairs.
      image: /img/expensive-repairs.svg
    - image: /img/tenant-issues.svg
      name: Tenant issues
      text:
        Facing late or partial payments? We understand managing tenants can be
        difficult, but we want to help!
    - image: /img/need-cash-fast.svg
      name: Need cash fast
      text:
        No matter what damage it may have, we’ll make a fair cash offer and you
        can expect prompt payment.
    - name: Need to relocate
      image: /img/need-relocate.svg
      text:
        Life changes may prompt a quick move. If that’s the case, we can close
        within a week with cash in your pocket.
    - name: Inherited home
      image: /img/inherited-home.svg
      text:
        An inherited home comes with additional hurdles and emotional ties. We
        will honor with a fair cash offer.
    - name: Avoid foreclosure
      image: /img/avoid-foreclosure.svg
      text:
        If you’re facing foreclosure, we can help stop the bank in its tracks with
        a fast and easy sale. No hidden fees.
testimonials:
  title: Testimonials
  heading: You’re not alone! In fact, we’ve helped thousands of sellers.
testimonialsHeading:
  title: Testimonials
  heading: You’re not alone! In fact, we’ve helped thousands of sellers.
---
