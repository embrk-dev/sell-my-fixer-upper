---
templateKey: projects-page
projects:
  - title: 16925 Triana Dr.
    state: Oklahoma
    text: This was originally a gorgeous demo home for one of the first builders in
      the neighborhood. Unfortunately, a tenant had had an accident and a fire
      caused significant damage. It’s now a wonderful home again and commands
      your attention upon entry!
    imagesBefore:
      - image: /img/06d70a_2d00c2fab377499886c61e552bd3a08c~mv2.webp
      - image: /img/06d70a_3b3c697efd30463cb3f750d5185a5a12~mv2.webp
      - image: /img/06d70a_18f32f91ab1a468bb65303da473d0211~mv2.webp
      - image: /img/06d70a_24ca6c3b34b04b2db2a8151e874c50c1~mv2.webp
      - image: /img/06d70a_39e0749cce3d40078bcd7b0795f1490c~mv2.webp
      - image: /img/06d70a_83fed782ec684fc190c0da94818d31a3~mv2.webp
      - image: /img/06d70a_8669d08e15b442b0817693253992b161~mv2.webp
      - image: /img/06d70a_598405de66a041998e8cfa76b7829f8a~mv2.webp
      - image: /img/06d70a_bca7c609cc1e4b89a530248e39f5f2df~mv2.webp
      - image: /img/06d70a_c1e785e8828440fda2e31eec3ff5a818~mv2.webp
      - image: /img/06d70a_cc4025cef52a4938b69a8acb043866fc~mv2.webp
    imagesAfter:
      - image: /img/06d70a_1ed70a484547433e85ada1952bf3503d~mv2.webp
      - image: /img/06d70a_0bbf0c80f24e47fbbc6eec76a9f1ce5a~mv2.webp
      - image: /img/06d70a_0daac09cbfaa45cb95dd0ae333012e90~mv2.webp
      - image: /img/06d70a_1df4c10fa61b431981246a6d8ae98e0e~mv2.webp
      - image: /img/06d70a_5e435b074f984e9585cb2511cfd26e8b~mv2.webp
      - image: /img/06d70a_24ca6c3b34b04b2db2a8151e874111c1~mv2.webp
      - image: /img/06d70a_0378c5d8b70b407c9fd129f44dc77ff3~mv2.webp
      - image: /img/06d70a_ad5dbdd015af42f084c08ea08cb416a0~mv2.webp
      - image: /img/06d70a_b357f66744da4acbbed6d07fa545d44f~mv2.webp
      - image: /img/06d70a_b318168ea73343478fd86611f4a0a7ab~mv2.webp
      - image: /img/06d70a_bce32af1735f4639a1df5e784090c91d~mv2.webp
  - title: 16000 Vicki Dr.
    state: Oklahoma
    text: This was a beautiful family home that had a fire. The owners had made the
      decision that they were not interested in dealing with all the repairs. We
      were happy to make it beautiful again!
    imagesBefore:
      - image: /img/06d70a_6a2a55160c74470590c54e523e0424ce~mv2.webp
      - image: /img/06d70a_6d8d2c458de94dc48355650906147714~mv2.webp
      - image: /img/06d70a_22c9201c63014757891a4d6815f1eee4~mv2.webp
      - image: /img/06d70a_45be4c2bc0df467fbe94c58a375d0187~mv2.webp
      - image: /img/06d70a_78f67f31c1b245b280204cee67529a3d~mv2.webp
      - image: /img/06d70a_4111ebff5420436fa186f4a6a5920d1a~mv2.webp
      - image: /img/06d70a_21676e94c37a42c3ae5bce7bba05e676~mv2.webp
      - image: /img/06d70a_021944b7d6e14929b9a4592d77d86464~mv2.webp
      - image: /img/06d70a_a096578780334c2f87db3c904078422c~mv2.webp
    imagesAfter:
      - image: /img/06d70a_5f915e684f6746bc8712dc35a52e0a15~mv2.webp
      - image: /img/06d70a_7afa90f1ae274c61bb8124b31ba06d37~mv2.webp
      - image: /img/06d70a_7d2f70b5fd8b4e0fa9f5926d786699c9~mv2.webp
      - image: /img/06d70a_8aa4475868ec4655b159b0df1aecf5e6~mv2.webp
      - image: /img/06d70a_064e0205961e41f18a79d1982d96430c~mv2.webp
      - image: /img/06d70a_671bf0508f324ad0ba947bc6cc823720~mv2.webp
      - image: /img/06d70a_83592d5a32fe413685f6e2e979aa8d07~mv2.webp
      - image: /img/06d70a_5050947f158843ed9d8a7edca79ab2b4~mv2.webp
      - image: /img/06d70a_b44b1b9b5eee44cdbb1296782b0873d0~mv2.webp
      - image: /img/06d70a_ca13f63d1319494885d65411e07129e8~mv2.webp
      - image: /img/06d70a_cb45e7a407944df7b35143de77ac27cc~mv2.webp
      - image: /img/06d70a_cd942ad1f624469ea4b4462bcb64a0a3~mv2.webp
      - image: /img/06d70a_ede2f8e5690c4dea94e4dba0403554d2~mv2.webp
  - title: 4513 SE 33rd St.
    state: Oklahoma
    text: This was originally another beautiful family home but had also sustained
      some significant damage. Transforming the home into something beautiful
      again is what we absolutely love doing!
    imagesBefore:
      - image: /img/06d70a_3ff6ce4e59f14bccb4b9c8eb538eeeb3~mv2.webp
      - image: /img/06d70a_8d1e4fcec2ac4e98b9fc406d39f35048~mv2.webp
      - image: /img/06d70a_9a478d439c784aa2a71e0f0f816581e5~mv2.webp
      - image: /img/06d70a_9c24dc5ee18e42a4a2758a8ccc3f35b2~mv2.webp
      - image: /img/06d70a_6991f7c22f9a4d25bd9b257d519079aa~mv2.webp
      - image: /img/06d70a_ce836a0704dc4fce9cedbdc78844ea82~mv2.webp
      - image: /img/06d70a_cf29713ada3f45efb84af3bf4ba36b5a~mv2.webp
      - image: /img/06d70a_f939590d25c7460091b7cc8d72b05f9e~mv2.webp
    imagesAfter:
      - image: /img/06d70a_01e88216e37e417e908e7062f07768f9~mv2.webp
      - image: /img/06d70a_7d181a21bae5412fa559d41584ec7d7b~mv2.webp
      - image: /img/06d70a_7ebca6ee82924190abec7b9ba65b54df~mv2.webp
      - image: /img/06d70a_552a51b0251141cd80bac30442413bfd~mv2.webp
      - image: /img/06d70a_d9d10f9e72e94eb98dbd65bf2e2facad~mv2.webp
      - image: /img/06d70a_df6d7dc958474c82ac6cf017e32cc6d9~mv2.webp
      - image: /img/06d70a_e7067d2a786f49209c765f24906476d6~mv2.webp
      - image: /img/06d70a_f03137cb2b44462c8be16b5f9fd07a00~mv2.webp
      - image: /img/06d70a_fe31a966802e466ab27d0be64476839c~mv2.webp
  - title: 2931 NW 20th St.
    state: Oklahoma
    text: This was an original 1920s beauty that had sat vacant for years. It had
      become home for the local vagrants, and the seller thought it was beyond
      repair. The home was actually scheduled to be demolished already when we
      got involved. It was our pleasure to restore the home back to its former
      glory!
    imagesBefore:
      - image: /img/screen-shot-2019-10-26-at-4_34_19-pm.webp
    imagesAfter:
      - image: /img/screen-shot-2019-10-26-at-4_34_42-pm.webp
  - title: 117 NW 28th St.
    state: Oklahoma
    text: This charming home in Jefferson Park sat vacant for many years! It was
      actually located in a historic area, which added another level of
      complication. We rebuilt from the footing to the roof. This is our ball of
      wax!
    imagesBefore:
      - image: /img/img_2666-480x360.webp
    imagesAfter:
      - image: /img/img_2147-1.webp
  - title: 1825 NW 37th St.
    state: Oklahoma
    text: This was previously an old rental that had seen years of abuse and
      neglect. It had finally hit its low point when the landlord called us. We
      specialize in the worst of the worst properties! We were able to renovate
      this home with lovely finishing touches!
    imagesBefore:
      - image: /img/screen-shot-2019-10-05-at-2_48_49-pm.webp
    imagesAfter:
      - image: /img/screen-shot-2019-10-05-at-2_49_10-pm.webp
  - title: "2425 NW 17th St. "
    state: Oklahoma
    text: This had been a family home for many years. Unfortunately, it had fallen
      into severe disrepair. We chose to take this one back to the studs to
      unleash the property’s full potential while adding some modern touches.
      The amazing end result is one of our favorites!
    imagesBefore:
      - image: /img/img_2414-1-480x360.webp
    imagesAfter:
      - image: /img/2425-nw-17th-300x200.webp
---
