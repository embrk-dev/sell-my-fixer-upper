---
path: /ourprojects
templateKey: ourprojects-page
seo:
  browserTitle: Sell My Fixer Upper
  metaTitle: Sell My Fixer Upper
  descriptionTitle: Sell My Fixer Upper
promo:
  title: Our Projects
  heading: Check out our
  headingMarked: recent work
  text: >-
    We buy homes, land, and apartment complexes all over the OKC metro area. We specialize in the worst of the worst! The love to tackle the biggest challenges out there whether that be a home in terrible disrepair, a completely vacant apartment building, or a very challenging piece of land…it’s our game.
---
