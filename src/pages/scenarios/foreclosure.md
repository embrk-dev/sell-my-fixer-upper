---
templateKey: scenario-page
seo:
  browserTitle: Sell My Fixer Upper
  metaTitle: Sell My Fixer Upper
  descriptionTitle: Sell My Fixer Upper
title: foreclosure
promo:
  heading: We buy homes for cash in Del City
  headingMarked: to give you the fresh start you desire
  promoList:
    - text: Close escrow on your timeline (as fast as 5 days)
    - text: No need to clean, stage, take pictures or have open houses
    - text: We pay for all repairs & closing costs
contentSectionUnderPromo:
  image: /img/img10.jpg
  heading: Proin luctus diam vel elit iaculis, non convallis ante accumsan lipsum.
  text: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis
    a diam sit amet lobortis. Sed faucibus enim vel sagittis ornare. Maecenas in
    mauris sed nibh tincidunt mattis nec at erat. Maecenas a ipsum non turpis
    iaculis dapibus. Donec ornare enim elementum metus auctor suscipit. Sed
    gravida nisi non commodo rhoncus. Maecenas leo arcu, fringilla sed tincidunt
    ut, cursus in risus. Mauris diam justo, varius quis lorem quis, iaculis
    ultricies leo.
  list:
    - text: Job Relocation
    - text: Personal or Family Issues
    - text: Financial Uncertainty or Short Sale
    - text: Job Relocation
    - text: Personal or Family Issues
    - text: Financial Uncertainty or Short Sale
bottomContentSection:
  list:
    - heading: Saving you time
      text: If you choose to accept our cash offer, we can close in as little as 5
        days.
      image: /img/save-time.svg
    - heading: Saving you money
      text: No need to worry about repairs or large restoration projects. We buy
        as-is.
      image: /img/save-money.svg
    - heading: Saving you hassle
      text: No closing costs or hidden fees. Agree to sell and we’ll take care of the
        rest.
      image: /img/save-hassle.svg
  title: LOREM IPSUM
  heading: Lorem ipsum dolor sit amet consectet
  text: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et
    rhoncus nulla, pharetra consectetur orci. In hac habitasse platea dictumst.
---
