---
templateKey: city-page
seo:
  browserTitle: Sell My Fixer Upper
  metaTitle: Sell My Fixer Upper
  descriptionTitle: Sell My Fixer Upper
title: Del City
promo:
  heading: We buy homes for cash in Del City
  headingMarked: to give you the fresh start you desire
  promoList:
    - text: Close escrow on your timeline (as fast as 5 days)
    - text: No need to clean, stage, take pictures or have open houses
    - text: We pay for all repairs & closing costs
contentSectionUnderPromo:
  image: /img/img10.jpg
  heading: Let Us Help You Sell Your Home In Del City, Oklahoma
  text: We will buy your property in Del City, Oklahoma, regardless of the
    conditions or the reasons for selling. Sell My Fixer Upper will conveniently
    buy your home with no obligation. Whether you face foreclosure, escaping bad
    tenants, having mortgage issues, or moving out, we’ll offer you the best
    cash price without the stress and inconveniences of home/property listing.
  list:
    - text: Job Relocation
    - text: Personal or Family Issues
    - text: Financial Uncertainty or Short Sale
    - text: Need Cash Fast
    - text: Tired of Managing Tenants
    - text: Home in Need of Costly Repairs
contentBlocks:
  list:
    - image: /img/sell-my-fixer-upper-any-condition.jpg
      title: We Make Selling Easy
      heading: Our Goal is To Help You Sell Quickly
      text: >-
        Our goal is to make the selling process convenient for ordinary
        homeowners looking to get rid of their property regardless of the
        conditions. Unlike the traditional selling process, we take care of
        repairs, commissions, and closing fees if any. This way, you can enjoy


        * Descent cash offer for your property 

        * A short seller wait time of 5 days or less 

        * Zero commission and no obligations 

        * Convenient closing date of your choosing
    - title: Open and Easy Process
      heading: Sell with No-Obligation 
      text: >-
        Selling your property doesn’t have to come with a string of obligations.
        We make the entire process as convenient for you as possible by
        accepting all homes in any condition – i.e., costly repairs, bad
        tenants, facing foreclosures, etc. Our no-obligation clause means:


        * We buy all property regardless of the conditions.

        * No need for expensive home inspections and showings 

        * No closing costs

        * No negotiations
      image: /img/sell-my-fixer-upper-cash-offer.jpg
    - title: Pick Your Closing Date
      heading: Putting You in Charge of the Selling Process
      text: >-
        One advantage of working with us is that you are always in charge of the
        selling process. Once you are ready to sell your home and have accepted
        the cash offer, you only need to give us the closing date, and we shall
        comply. As part of the process:


        * We make the cash offer 

        * You decide if it’s good enough and communicate back to us

        * If you are comfortable with the offer, you pick the closing date

        * No contingencies, obligations, or hidden fees
      image: /img/sell-my-fixer-upper-close-date.jpg
mapBlock:
  title: Get A Cash Offer
  heading: Sell your home in Del City
  text: Del City, Oklahoma, is part of the Oklahoma City metropolitan area located
    near the two major interstate highways. With several businesses and places
    to explore, this neighborhood is home to a string of eateries, drinking
    joints, sightings, and museums, as well as leisure and outdoor parks.
bottomContentSection:
  list:
    - heading: Saving you time
      text: If you choose to accept our cash offer, we can close in as little as 5
        days.
      image: /img/save-time.svg
    - heading: Saving you money
      text: No need to worry about repairs or large restoration projects. We buy
        as-is.
      image: /img/save-money.svg
    - heading: Saving you hassle
      text: No closing costs or hidden fees. Agree to sell and we’ll take care of the
        rest.
      image: /img/save-hassle.svg
  title: "GET YOUR QUOTE TODAY "
  heading: We make selling your fixer upper easy
  text: Sell My Fixer Upper can help you sell your property, no matter why you’re
    selling.  So reach out today and get your cash offer.
---
