---
templateKey: sellwithconfidence-page
sellwithconfidence:
  title: Sell with Confidence
  heading: Why over 1000+ people have sold to Sell My Fixer Upper
  table:
    - rowTitle: Avg. time seller waits for closing
      smfuText: 5 days or less
      taText: 90+ days
    - rowTitle: Who pays for seller’s repairs
      smfuText: Sell My Fixer Upper
      smfuTooltipText: We buy homes in any condition, as-is
      taText: You do
    - rowTitle: Avg. number of showings
      smfuText: '0'
      smfuTooltipText: We buy homes in any condition, as-is
      taText: 25+
    - smfuTooltipText: We buy homes in any condition, as-is
      taText: You do
      smfuText: Sell My Fixer Upper
      rowTitle: Who pays for closing costs
    - smfuTooltipText: ''
      rowTitle: Avg. amount seller wastes on commissions
      smfuText: $0
      taText: $30,000*
    - smfuTooltipText: We buy homes in any condition, as-is
      rowTitle: Are there any contingencies
      smfuText: None
      taText: Many
    - rowTitle: Will seller receive a cash offer
      smfuTooltipText: We buy homes in any condition, as-is
      smfuText: Always
      taText: Rarely
    - smfuTooltipText: We buy homes in any condition, as-is
      rowTitle: Who is responsible for bad tenants
      smfuText: Sell My Fixer Upper
      taText: You are
    - smfuTooltipText: We buy homes in any condition, as-is
      taText: Others do
      smfuText: You do
      rowTitle: Who decides the closing date
---
