---
templateKey: testimonials-page
testimonialsList:
  - text: Justin and his team are exceptional at what they do. Justin is fair and
      honest and I would recommend him to anyone wanting to sell, lease or sell
      propreties.
    image: /img/testimonials1.jpg
    name: Amine Elmes
    whenPosted: September 14, 2019
  - text: Justin does great work and is a pleasure to deal with. His ethics are
      exceptional and his team knows their business.
    image: /img/testimonials2.jpg
    name: Edward C Lorenzen
    whenPosted: January 5, 2019
---
