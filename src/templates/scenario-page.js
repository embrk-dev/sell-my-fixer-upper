import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import ReactMarkdown from 'react-markdown';

import "../styles/city.scss"

import Seo from '../components/Seo'
import Layout from '../components/Layout'
import Lightbox from "../components/Lightbox"
import Testimonials from '../components/Testimonials'
import Sellwithconfidence from '../components/SellWithConfidence'
import Faq from '../components/Faq'

import checkGreen from '../img/check-green.svg'
import property from '../img/property.svg'
import cashOffer from '../img/cash-offer.svg'
import calendar from '../img/calendar.svg'

export const ScenarioPageTemplate = ({
  promoData,
  contentsectionunderpromoData,
  bottomcontentsectionData,
}) => (
  <>
    <div className="max-w-page mx-auto px-8 border-box">
      <div className="flex lg:flex-row flex-col lg:items-center justify-between relative z-10 mt-24 sm:mb-40 mb-20">
        <div className="lg:mb-0 mb-12">
          <h2 className="lg:text-54 text-34 lg:mb-16 mb-8 font-extrabold text-dark-gray leading-normal">
            {promoData.heading}
            <strong className="text-blue"> {promoData.headingMarked}</strong>
          </h2>
          <ul className="lg:text-24 text-20">
            {promoData.promoList.map((node, i) => (
              <li key={i} className="pb-4">
                <img
                  className="mr-6 inline-block align-middle"
                  src={checkGreen}
                  alt=""
                  style={{ width: '22px', height: '18px' }}
                />
                {node.text}
              </li>
            ))}
          </ul>
        </div>
        <div className="lg:ml-28 lg:w-474 lg:mx-0 -mx-8 border-box flex-shrink-0 bg-light-gray sm:p-20 p-8 border-box lg:rounded-10 text-center">
          <div className="max-w-xl mx-auto lg:max-w-auto lg:mx-0">
            <h3 className="font-extrabold text-dark-gray sm:text-36 text-24 sm:mb-24 mb-16">
              It’s as easy as 1,2,3
            </h3>
            <ul className="text-dark-gray md:text-20 text-16 font-medium pb-14">
              <li className="flex items-center mb-10">
                <img
                  className="ml-4 mr-10"
                  src={property}
                  alt="1. Tell us about the property"
                  style={{ width: '66px', height: '50px' }}
                />
                1. Tell us about the property
              </li>
              <li className="flex items-center mb-10">
                <img
                  className="ml-4 mr-6"
                  src={cashOffer}
                  alt="2. Receive a fair, cash offer"
                  style={{ width: '75px', height: '37px' }}
                />
                2. Receive a fair, cash offer
              </li>
              <li className="flex items-center mb-10">
                <img
                  className="ml-4 mr-10"
                  src={calendar}
                  alt="3. Close when you’re ready"
                  style={{ width: '63px', height: '44px' }}
                />
                3. Close when you’re ready
              </li>
            </ul>
            <Lightbox btnClass="py-8 leading-none font-bold rounded-50 text-white bg-blue px-20 text-18 sm:w-auto w-full" />
          </div>
        </div>
      </div>
      <div className="lg:mb-60 pb-32 mb-32">
        <div className="flex lg:flex-row flex-col-reverse justify-between items-center lg:pb-28 pb-10">
          <div className="lg:w-1/2 w-full">
            {contentsectionunderpromoData.image.childImageSharp ? (
              <Img
                fluid={contentsectionunderpromoData.image.childImageSharp.fluid}
                className="rounded-10 overflow-hidden"
                alt=""
              />
            ) : (
              <img
                src={contentsectionunderpromoData.image.publicURL}
                className="rounded-10"
                alt=""
              />
            )}
          </div>
          <div className="lg:w-flex1/2 border-box lg:pl-8 leading-normal text-18 lg:mb-0 mb-10">
            <h3 className="font-extrabold text-dark-gray md:text-36 text-24 mb-8 leading-tight">
              {contentsectionunderpromoData.heading}
            </h3>
            <ReactMarkdown source={contentsectionunderpromoData.text} />
          </div>
        </div>
        <div className="max-w-1100 mx-auto md:text-24 sm:text-18 text-16 flex md:flex-row flex-col justify-between md:pb-24 pb-10">
          <ul className="cols-2 mx-auto">
            {contentsectionunderpromoData.list.map((node, i) => (
              <li key={i} className="pb-4">
                <img
                  className="mr-6 inline-block align-middle"
                  src={checkGreen}
                  alt=""
                  style={{ width: '22px', height: '18px' }}
                />
                {node.text}
              </li>
            ))}
          </ul>
        </div>
        <div className="text-center">
          <Lightbox btnClass="py-8 leading-none font-bold rounded-50 text-white bg-blue px-20 text-18 w-full max-w-xl" />
        </div>
      </div>
    </div>

    <div className="city-content-holder bg-light-gray lg:mt-66 mt-24 relative sm:pb-40 pb-20">
      <div className="max-w-page mx-auto px-8 border-box lg:pt-40 pt-24">
        <div className="lg:pb-16">
          <Sellwithconfidence />
        </div>

        <Testimonials showHeader={false} />
      </div>
    </div>
    <div className="max-w-page mx-auto px-8 border-box sm:pt-40 pt-20 lg:pb-60 pb-24">
      <div className="max-w-91 mx-auto lg:pb-36">
        <Faq />
      </div>
      <div className="max-w-1100 mx-auto">
        <div className="md:text-center pt-12 pb-5 max-w-80 mx-auto pb-24">
          <h2 className="uppercase font-semibold md:text-18 text-14 text-blue block mb-4">
            {bottomcontentsectionData.title}
          </h2>
          <h3 className="md:text-36 text-24 font-extrabold mb-6 text-dark-gray">
            {bottomcontentsectionData.heading}
          </h3>
          <p className="md:text-20 text-16">{bottomcontentsectionData.text}</p>
        </div>
        <div className="flex text-center justify-center items-center md:flex-row flex-col md:px-0 px-8 md:mb-12">
          {bottomcontentsectionData.list.map((node, i) => (
            <div key={i} className="md:w-1/3 md:px-8 md:md-0 mb-20">
              <div className="h-60">
                <img
                  className="inline-block"
                  src={node.image.publicURL}
                  alt={node.heading}
                />
              </div>
              <h5 className="text-24 font-extrabold mb-2 text-dark-gray leading-snug">
                {node.heading}
              </h5>
              <p>{node.text}</p>
            </div>
          ))}
        </div>
        <div className="text-center">
          <Lightbox btnClass="py-8 leading-none font-bold rounded-50 text-white bg-blue px-20 text-18 md:w-auto w-full" />
        </div>
      </div>
    </div>
  </>
);

ScenarioPageTemplate.propTypes = {
  title: PropTypes.string,
}
const ScenarioPage = ({ data }) => {
  const { frontmatter: post } = data.pageData

  return (
    <React.Fragment>
      <Seo data={post.seo}></Seo>
      <Layout>
        <ScenarioPageTemplate
          title={post.title}
          promoData={post.promo}
          contentsectionunderpromoData={post.contentSectionUnderPromo}
          bottomcontentsectionData={post.bottomContentSection}
        />
      </Layout>
    </React.Fragment>
  );
}

ScenarioPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default ScenarioPage

export const ScenarioPageQuery = graphql`
  query ScenarioByID($id: String!) {
    pageData: markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        title
        seo {
          browserTitle
          metaTitle
          descriptionTitle
        }
        promo {
          heading
          headingMarked
          promoList {
            text
          }
        }
        contentSectionUnderPromo {
          image {
            childImageSharp {
              fluid(maxWidth: 650, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
            publicURL
          }
          heading
          text
          list {
            text
          }
        }
        bottomContentSection {
          title
          heading
          text
          list {
            heading
            text
            image {
              publicURL
            }
          }
        }
      }
    }
  }
`;
