import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Layout from '../components/Layout'
import Testimonials from '../components/Testimonials'
import Sellwithconfidence from '../components/SellWithConfidence'
import Lightbox from "../components/Lightbox"
import ReactMarkdown from 'react-markdown'
import Seo from '../components/Seo'
import Img from 'gatsby-image'
import { graphql } from 'gatsby'

import video from '../img/file.mp4'
import property from '../img/property.svg'
import cashOffer from '../img/cash-offer.svg'
import calendar from '../img/calendar.svg'

function IndexPageTemplate({
  promoData,
  howitworksData,
  ourpromiseData,
}) {

  const [hiddenPoster, hide] = useState(false)

  function hidePoster() {
    hide(true);
  }

  return (
    <Layout>
      <div id="about-us-section" className="lg:py-48 lg:mb-28 sm:pb-20 sm:pt-20 pb-12 pt-8 sm:mb-0 mb-16 relative">
        <div className="max-w-page mx-auto px-8 border-box flex lg:flex-row flex-col items-center justify-between relative z-10">
          <div className="lg:w-1/2 lg:text-left md:text-24 sm:text-20 text-16 text-center">
            <strong className="sm:text-18 uppercase font-semibold block mb-6 text-14">{promoData.title}</strong>
            <h2 className="md:text-64 text-34 mb-6 font-extrabold text-dark-gray leading-tight">{promoData.heading} <strong className="block text-blue">{promoData.headingMarked}</strong></h2>
            <ReactMarkdown className="md:mb-14 mb-8" source={promoData.text}/>
            <Lightbox btnClass="py-8 leading-none font-bold rounded-50 text-white bg-blue px-20 text-18 sm:w-auto w-full"/>
          </div>
          <div className="lg:w-1/2 border-box lg:pl-12">
            {promoData.image.childImageSharp
              ? <div
                  onClick={hidePoster}
                  onKeyDown={hidePoster}
                  className={`${hiddenPoster ? 'hidden' : 'block'} w-full h-auto`}>
                  <Img
                    fluid={promoData.image.childImageSharp.fluid}
                    alt="We Buy Homes In Any Condition"
                  />
                </div>
              : <img
                  onClick={hidePoster}
                  onKeyDown={hidePoster}
                  className={`${hiddenPoster ? 'hidden' : 'block'} w-full h-auto`}
                  src={promoData.image.publicURL}
                  alt="We Buy Homes In Any Condition"
                />
            }
            <video autoPlay = {hiddenPoster ? 'true' : undefined} className={`${hiddenPoster ? 'block' : 'hidden'}`}  controls src={video}></video>
          </div>
        </div>
      </div>
      <div className="max-w-content mx-auto px-8 border-box">
        <div className="md:pb-48 pb-6 flex lg:flex-row flex-col items-center justify-between">
          <div className="max-w-4xl w-full md:block hidden">
            {howitworksData.image.childImageSharp
              ? <Img
                  fluid={howitworksData.image.childImageSharp.fluid}
                  alt={howitworksData.text}
                />
              : <img
                  className="block w-full h-auto"
                  src={howitworksData.image.publicURL}
                  alt={howitworksData.text}
                />
            }
          </div>
          <div className="lg:w-1/2 border-box lg:pl-8">
            <strong className="uppercase font-semibold md:text-18 text-14 text-blue block mb-4">{ howitworksData.title}</strong>
            <h3 className="md:text-38 text-24 font-extrabold mb-6 text-dark-gray">{howitworksData.heading}</h3>
            <ReactMarkdown className="md:mb-10 mb-20 leading-relaxed" source={howitworksData.text}/>
            <ul className="text-dark-gray md:text-20 text-16 font-medium">
              <li className="flex items-center mb-10">
                <img
                  className="ml-4 mr-10"
                  src={property}
                  alt="1. Tell us about the property"
                  style={{ width: '66px', height: '50px' }}
                />
                1. Tell us about the property
              </li>
                <li className="flex items-center mb-10">
                  <img
                    className="ml-4 mr-6"
                    src={cashOffer}
                    alt="2. Receive a fair, cash offer"
                    style={{ width: '75px', height: '37px' }}
                  />
                2. Receive a fair, cash offer
              </li>
                <li className="flex items-center mb-10">
                  <img
                    className="ml-4 mr-10"
                    src={calendar}
                    alt="3. Close when you’re ready"
                    style={{ width: '63px', height: '44px' }}
                  />
                3. Close when you’re ready
              </li>
            </ul>
          </div>
        </div>

        <Sellwithconfidence />

        <div className="md:pb-24 pb-4">
          <div className="md:text-center md:pb-28 pb-12 max-w-80 mx-auto">
            <strong className="uppercase font-semibold md:text-18 text-14 text-blue block mb-4">{ ourpromiseData.title}</strong>
            <h3 className="md:text-38 text-24 font-extrabold mb-6 text-dark-gray">{ourpromiseData.heading}</h3>
            <ReactMarkdown className="md:text-20 text-16" source={ourpromiseData.text}/>
          </div>
          <div className="flex flex-wrap justify-between lg:px-16">
            {ourpromiseData.ourpromiseList.map((node, i) => (
            <div key={i} className="md:w-138 md:pb-24 w-full pb-12 flex">
              <span className="w-16 flex-shrink-0">
                  <img
                    src={node.image.publicURL}
                    alt={node.name}
                  />
              </span>
              <div className="leading-relaxed">
                <h4 className="font-medium text-19 text-dark-gray mb-3">{node.name}</h4>
                <p>{node.text}</p>
              </div>
            </div>
            ))}
          </div>
        </div>
      </div>
      <div className="max-w-page sm:px-8 px-6 mx-auto relative z-10">
        <Testimonials />
      </div>
    </Layout>
  )
}

const IndexPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <React.Fragment>
      <Seo data={frontmatter.seo}></Seo>
      <IndexPageTemplate
        promoData={frontmatter.promo}
        howitworksData={frontmatter.howitworks}
        ourpromiseData={frontmatter.ourpromise}
      />
    </React.Fragment>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "index-page" } }) {
      frontmatter {
        seo {
          browserTitle
          metaTitle
          descriptionTitle
        }
        promo {
          title
          heading
          headingMarked
          text
          image{
            childImageSharp {
              fluid(maxWidth:613, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
            publicURL
          }
        }
        howitworks {
          title
          heading
          text
          image{
            childImageSharp {
              fluid(maxWidth:540, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
            publicURL
          }
        }
        ourpromise {
          title
          heading
          text
          ourpromiseList{
            image{
              publicURL
            }
            name
            text
          }
        }
      }
    }
  }
`
