import React from 'react'
import Layout from '../components/Layout'
import Faq from '../components/Faq'
import Testimonials from '../components/Testimonials'
import Lightbox from "../components/Lightbox"
import "../styles/howitworks.scss"

import property from '../img/property.svg'
import cashOffer from '../img/cash-offer-about.svg'
import calendar from '../img/calendar.svg'

export const HowitworksPageTemplate = () => (
  <React.Fragment>
    <div className="md:text-center md:pt-28 pt-12 pb-5 max-w-80 mx-auto px-8">
      <h2 className="uppercase font-semibold md:text-18 text-14 mb-4">How It Works</h2>
      <h3 className="md:text-54 text-24 font-extrabold mb-6 text-dark-gray">Selling is <span className="text-blue">easy as 1, 2, 3</span></h3>
      <p className="md:text-20 text-16">With our 3-step process, we will save you time and money. Our process allows you to sell your home quickly and at full value no matter its condition.</p>
    </div>
    <div className="mx-auto px-8 lg:pb-32 pb-16" style={{ maxWidth: '115.2rem' }}>
      <div id="steps" className="relative z-10 lg:pt-40 pt-8">
        <div className="step lg:mb-36 mb-16 flex items-center justify-between lg:flex-row flex-col-reverse">
          <div style={{ maxWidth: '51rem' }} className="lg:pt-0 lg:text-left text-center pt-12">
            <strong className="uppercase font-semibold md:text-18 text-14 text-blue block mb-4">Step 1</strong>
            <h3 className="md:text-36 text-24 font-extrabold mb-6 text-dark-gray">Tell us about the property</h3>
            <p className="text-18 lg:mb-10 mb-0 leading-relaxed">Begin by <Lightbox btnClass="cursor-pointer text-blue" btnText="filling out this offer form" /> with your contact information and property address. If you prefer, give us a call or text us anytime at 1-844-735-5693.</p>
          </div>
          <div className="img relative z-10 flex justify-center lg:w-flex1/2 w-full">
            <span className="rounded-full flex flex-shrink-0 items-center bg-light-gray lg:w-144 w-88 lg:h-144 h-88">
              <img
                className="lg:ml-16 flex-shrink-0 property"
                src={ property }
                alt="Tell us about the property"
              />
            </span>
          </div>
        </div>
        <div className="step lg:mb-36 mb-16 flex items-center justify-between lg:flex-row-reverse flex-col-reverse">
          <div style={{ maxWidth: '51rem' }} className="lg:pt-0 lg:text-left text-center pt-12 pl-14">
            <strong className="uppercase font-semibold md:text-18 text-14 text-blue block mb-4">Step 2</strong>
            <h3 className="md:text-36 text-24 font-extrabold mb-6 text-dark-gray">Receive your cash offer</h3>
            <p className="text-18 lg:mb-10 mb-0 leading-relaxed">As soon as we receive the request, we will contact you to learn more about your needs and desired timeline. At most, all we need is 5 minutes inside the house for a walkthrough. If this is not possible- no problem! We can still make an obligation-free, cash offer with just a few photos and your address. No strings attached.</p>
          </div>
          <div className="img relative z-10 flex justify-center w-flex1/2">
            <span className="rounded-full flex flex-shrink-0 items-center bg-light-gray lg:w-144 w-88 lg:h-144 h-88">
              <img
                className="ml-12 flex-shrink-0"
                src={ cashOffer }
                alt="Receive your cash offer"
                style={{ width: '367px' }}
              />
            </span>
          </div>
        </div>
        <div className="step lg:mb-28 mb-16 flex items-center justify-between lg:flex-row flex-col-reverse">
          <div style={{ maxWidth: '51rem' }} className="lg:pt-0 lg:text-left text-center pt-12">
            <strong className="uppercase font-semibold md:text-18 text-14 text-blue block mb-4">Step 3</strong>
            <h3 className="md:text-36 text-24 font-extrabold mb-6 text-dark-gray">Choose your closing date</h3>
            <p className="text-18 lg:mb-10 mb-0 leading-relaxed">If you choose to accept our offer, we have the ability to close within days. We’ll walk you through our simplified process and you choose the closing date that works best for you. We’ll even take care of the closing costs.</p>
          </div>
          <div className="img relative z-10 flex justify-center w-flex1/2">
            <span className="rounded-full flex flex-shrink-0 items-center bg-light-gray lg:w-144 w-88 lg:h-144 h-88">
              <img
                className="ml-5 mt-12 flex-shrink-0"
                src={ calendar }
                alt="Choose your closing date"
                style={{ width: '366px' }}
              />
            </span>
          </div>
        </div>
        <div className="text-center relative z-10">
          <Lightbox btnClass="py-8 leading-none font-bold rounded-50 text-white bg-blue px-8 text-18 max-w-xl w-full inline-block" btnText="Request your Cash Offer" />
        </div>
      </div>
    </div>

    <div className="testimonials-holder bg-light-gray sm:mt-66 mt-36 relative pb-48">
      <div className="max-w-page sm:px-8 px-6 mx-auto relative z-10 sm:pb-28 pb-12">
        <Testimonials />
      </div>
      <div className="max-w-98 px-8 mx-auto md:pb-48 pb-20">
        <Faq/>
      </div>
    </div>

  </React.Fragment>
)


const HowitworksPage = () => {
  return (
    <Layout>
      <HowitworksPageTemplate />
    </Layout>
  )
}

export default HowitworksPage
