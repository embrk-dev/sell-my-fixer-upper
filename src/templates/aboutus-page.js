import React from 'react'
import Layout from '../components/Layout'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown'
import Seo from '../components/Seo'
import Img from 'gatsby-image'
import { graphql } from 'gatsby'
import "../styles/about.scss"

import saveTime from '../img/save-time.svg'
import saveMoney from '../img/save-money.svg'
import saveHassle from '../img/save-hassle.svg'

export const AboutusPageTemplate = ({
  promoData,
  aboutusData,
  displayTeamData,
  ourTeamData,
}) => (
  <React.Fragment>
    <div className="text-center md:pt-28 pt-12 md:pb-24 pb-8 max-w-80 mx-auto px-8">
      <h2 className="uppercase font-semibold md:text-18 text-14 mb-4">{ promoData.title }</h2>
      <h3 className="md:text-54 text-34 font-extrabold mb-6 text-dark-gray">{ promoData.heading } <span className="text-blue">{ promoData.headingMarked }</span></h3>
    </div>
    <div className="max-w-page mx-auto px-8 pb-60">
      <div className="flex justify-between lg:pb-44 lg:flex-row flex-col">
        <div className="md:w-1/2 w-full mx-auto mb-12">
          {aboutusData.image.childImageSharp
            ? <Img
                className="block w-full h-auto rounded-10"
                fluid={aboutusData.image.childImageSharp.fluid}
                alt={aboutusData.text}
              />
            : <img
                className="block w-full h-auto rounded-10"
                src={aboutusData.image.publicURL}
                alt={aboutusData.text}
              />
          }
        </div>
        <div className="border-box lg:w-1/2 lg:pl-16 lg:pr-20 md:text-18 text-16 mb-12">
          <h4 className="md:text-36 text-24 font-extrabold mb-8 text-dark-gray leading-snug">{aboutusData.heading}</h4>
          <ReactMarkdown className="aboutus-text" source={aboutusData.text}/>
        </div>
      </div>

      <div className="flex text-center justify-center items-center md:flex-row flex-col md:px-0 px-8 md:mb-12">
        <div className="md:w-1/3 md:px-8 md:md-0 mb-20">
          <div className="h-60">
            <img className="inline-block" src={saveTime} style={{ width: '57px', height: '108px' }} alt="Saving you time"/>
          </div>
          <h5 className="text-24 font-extrabold mb-2 text-dark-gray leading-snug">Saving you time</h5>
          <p>If you choose to accept our cash offer, we can close in as little as 5 days.</p>
        </div>
        <div className="md:w-1/3 md:px-8 md:md-0 mb-20">
          <div className="h-60">
            <img className="inline-block" src={saveMoney} style={{ width: '111px', height: '113px' }} alt="Saving you money"/>
          </div>
          <h5 className="text-24 font-extrabold mb-2 text-dark-gray leading-snug">Saving you money</h5>
          <p>No need to worry about repairs or large restoration projects. We buy as-is.</p>
        </div>
        <div className="md:w-1/3 md:px-8 md:md-0 mb-20">
          <div className="h-60">
            <img className="inline-block" src={saveHassle} style={{ width: '96px', height: '110px' }} alt="Saving you hassle"/>
          </div>
          <h5 className="text-24 font-extrabold mb-2 text-dark-gray leading-snug">Saving you hassle</h5>
          <p>No closing costs or hidden fees. Agree to sell and we’ll take care of the rest. </p>
        </div>
      </div>
    </div>
    {displayTeamData
      ?
      <div className="our-team bg-light-gray pt-80 pb-66 -mt-60 sm:px-8 px-4">
        <div className="max-w-7xl mx-auto">
          <div className="text-center max-w-5xl mx-auto pb-28">
            <h4 className="md:text-36 text-24 font-extrabold mb-8 text-dark-gray leading-snug">{ourTeamData.heading}</h4>
            <ReactMarkdown className="text-18" source={ourTeamData.text}/>
          </div>
          {ourTeamData.teamMembers.map((node, i) => (
          <div key={i} className="flex sm:flex-row flex-col mb-16">
            {node.photo.childImageSharp
              ? <Img
                  className="flex-shrink-0 mx-auto sm:mx-0"
                  fluid={node.photo.childImageSharp.fluid}
                  alt={node.name}
                  style={{ width: '224px', height: '288px' }}
                />
              : <img
                  className="flex-shrink-0 mx-auto sm:mx-0"
                  src={node.photo.publicURL}
                  alt={node.name}
                  style={{ width: '224px', height: '288px' }}
                />
            }
            <div className="flex flex-col sm:pl-8 justify-center text-18">
              <h5 className="text-20 font-semibold text-dark-gray leading-snug mb-2">{ node.name }</h5>
              <h6 className="mb-4 text-blue text-20 font-normal">{node.position}</h6>
              <ReactMarkdown className="mb-8" source={node.text}/>
            </div>
          </div>
          ))}
        </div>
      </div>
      : ''
    }

  </React.Fragment>
)


const AboutusPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark
  return (
    <React.Fragment>
      <Seo data={frontmatter.seo}></Seo>
      <Layout>
        <AboutusPageTemplate
          promoData={frontmatter.promo}
          aboutusData={frontmatter.aboutus}
          displayTeamData={frontmatter.displayTeam}
          ourTeamData={frontmatter.ourTeam}
        />
      </Layout>
    </React.Fragment>
  )
}

AboutusPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default AboutusPage

export const pageQuery = graphql`
  query AboutusPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "aboutus-page" } }) {
      frontmatter {
        seo {
          browserTitle
          metaTitle
          descriptionTitle
        }
        promo {
          title
          heading
          headingMarked
        }
        aboutus {
          heading
          text
          image{
            childImageSharp {
              fluid(maxWidth:650, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
            publicURL
          }
        }
        displayTeam
        ourTeam {
          heading
          text
          teamMembers{
            photo{
              childImageSharp {
                fluid(maxWidth:224, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
              publicURL
            }
            name
            position
            text
          }
        }
      }
    }
  }
`
