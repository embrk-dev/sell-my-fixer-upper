import React from 'react'
import Layout from '../components/Layout'
import PropTypes from 'prop-types'
import "../styles/styles.scss"
import { useStaticQuery, graphql } from 'gatsby'
import Project from "../components/Project"
import "../styles/ourprojects.scss"
import Seo from '../components/Seo'
import ReactMarkdown from 'react-markdown'

export const OurprojectsTemplate = ({ data, promoData }) => (
  <React.Fragment>
    <div className="md:text-center md:pt-28 pt-12 max-w-80 mx-auto sm:px-8 px-4 md:pb-40 pb-8">
      <h2 className="uppercase font-semibold md:text-18 text-14 mb-4">
        {promoData.title}
      </h2>
      <h3 className="md:text-54 text-24 font-extrabold mb-6 text-dark-gray">
        {promoData.heading}{' '}
        <span className="text-blue">{promoData.headingMarked}</span>
      </h3>
      <ReactMarkdown className="md:text-20 text-16" source={promoData.text} />
    </div>
    <div className="pb-60 max-w-80 mx-auto sm:px-8 px-4">
      {data.map((node, index) => (
        <div key={index} className="md:mb-40 mb-20">
          <Project data={node} />
        </div>
      ))}
    </div>
  </React.Fragment>
);

const OurprojectsPage = () => {
  const pagedata = useStaticQuery(graphql`
    query {
      projectsData: markdownRemark(
        frontmatter: { templateKey: { eq: "projects-page" } }
      ) {
        frontmatter {
          projects {
            title
            state
            imagesBefore {
              image {
                childImageSharp {
                  fluid(maxWidth: 1200, quality: 100) {
                    ...GatsbyImageSharpFluid
                  }
                }
                publicURL
              }
            }
            imagesAfter {
              image {
                childImageSharp {
                  fluid(maxWidth: 1200, quality: 100) {
                    ...GatsbyImageSharpFluid
                  }
                }
                publicURL
              }
            }
            text
          }
        }
      }

      AboutusPageTemplate: markdownRemark(
        frontmatter: { templateKey: { eq: "ourprojects-page" } }
      ) {
        frontmatter {
          seo {
            browserTitle
            metaTitle
            descriptionTitle
          }
          promo {
            title
            heading
            headingMarked
            text
          }
        }
      }
    }
  `);

  const projects = pagedata.projectsData.frontmatter.projects;
  const { frontmatter } = pagedata.AboutusPageTemplate;

  return (
    <React.Fragment>
      <Seo data={frontmatter.seo}></Seo>
      <Layout>
        <OurprojectsTemplate
          data={projects}
          promoData={frontmatter.promo}
        />
      </Layout>
    </React.Fragment>
  );
};

export default OurprojectsPage
